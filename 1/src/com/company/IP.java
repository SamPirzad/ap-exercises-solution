package com.company;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IP {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        // this part is for regex method
        String part = "([01]?\\d{1,2}|2[0-4]\\d|25[0-5])";
        Pattern pattern = Pattern.compile("^(" + part + "\\.)"
                + "{3}" + part + "$");

        for(; n > 0; n--) {
            String ip = in.next();
            System.out.println("regex method : "
                    + (checkIPRegex(ip, pattern) ? "valid" : "invalid"));
            System.out.println("second method : "
                    + (checkIP(ip) ? "valid" : "invalid"));
        }
    }

    static boolean checkIPRegex(String ip, Pattern pattern) {
        Matcher matcher = pattern.matcher(ip);
        return matcher.find();
    }

    static boolean checkIP(String ip) {
        if (ip.charAt(0) == '.' || ip.charAt(ip.length() - 1) == '.')
            return false;
        int counter = 0;
        for (int i = 1; i < ip.length() - 2; i++){
            if (ip.charAt(i) == ip.charAt(i + 1) && ip.charAt(i) == '.')
                return false;
            else if (ip.charAt(i) == '.')
                counter++;
        }
        if (counter != 3)
            return false;

        String[] parts = ip.split("\\.");

        for (String part : parts){
            if (part.length() > 3)
                return  false;
            for(int i = 0; i < part.length(); i++){
                if (part.charAt(i) < '0' || part.charAt(i) > '9')
                    return  false;
            }
            int value = Integer.parseInt(part);
            if (value < 0 || value > 255)
                return false;
        }
        return true;
    }
}
