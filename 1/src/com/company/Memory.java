package com.company;

import java.util.Scanner;

public class Memory {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] arr = new int[n];
        String query = in.next();
        int itr = 0;
        for(int i = 0; i < query.length(); i++) {
            switch (query.charAt(i)) {
            case '+':
                arr[itr]++;
                break;
            case '-':
                arr[itr]--;
                break;
            case '>':
                itr = (itr + 1) % n;
                break;
            case '<':
                itr = (itr + n - 1) % n;
                break;
            }
        }
        for(int i = 0; i < n; i++)
            System.out.print(arr[i] + " ");
    }
}
