package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Sort {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        ArrayList<String> arr = new ArrayList<>();
        for(int i = 0; i < n; i++)
            arr.add(in.next());
        Collections.sort(arr);
        System.out.println(arr.get(0));
        for(int i = 1; i < n; i++) {
            if(!arr.get(i).equals(arr.get(i-1)))
                System.out.println(arr.get(i));
        }
    }
}
