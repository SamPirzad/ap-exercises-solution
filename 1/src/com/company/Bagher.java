package com.company;

import java.util.Scanner;

public class Bagher {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int[] angles = new int[3];
        int res = 0;
        boolean isValid = true;
        for (int i = 0; i < 3; i++) {
            angles[i] = in.nextInt();
            res += angles[i];
            if ( angles[i] == 0 )
                isValid = false;
        }
        System.out.println((isValid && res == 180) ? "Yes" : "No");
    }
}
